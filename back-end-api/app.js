//[SECTION] Dependencies and Modules
	const express = require('express'); 
	const mongoose = require('mongoose'); 
	const cors = require('cors');
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');

//[SECTION] Environment Setup
    let account = "mongodb+srv://admin2:admin2@wdc028-course-booking.ywk7a.mongodb.net/CSP3?retryWrites=true&w=majority";
	const port = 4000; 

//[SECTION] Server Setup
	const app = express();
	app.use(express.json());
	//it enables all origins/address/URL of the client request
	app.use(cors());


//[SECTION] Database Connection 
	mongoose.connect(account);
	const connectStatus = mongoose.connection; 
	connectStatus.once('open', () => console.log(`Database Connected`));


//[SECTION] Backend Routes 
	//http://localhost:4000/users
	app.use('/users', userRoutes); 
	//http://localhost:4000/courses
	app.use('/products', productRoutes); 

	
//[SECTION] Server Gateway Response
	app.get('/', (req, res) => {
		res.send('Welcome to Enrollment-System')
	}); 
	app.listen(port, () => {
		console.log(`API is Hosted port ${port}`);
	}); 