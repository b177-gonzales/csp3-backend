const Product = require('../models/Product');


//Create a new course
/*
Steps:
	1. Create a new Course object using the mongoose model and the information from the request body.
	2. Save the new Course to the database
*/
module.exports.addProduct = (reqBody) => {

	//create a variable "newCourse" and instantiate the name, description, price
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	//Save the created object to our database
	return newProduct.save().then((product, error ) => {
		//Course creation is successful or not
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	}).catch(error => error)
}


//Retrieve all ACTIVE courses
//1. Retrieve all the courses with the property isActive: true

//users who aren't logged in should also be able to view the courses
module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then(result => {
		return result;
	}).catch(error => error)
}


//Retrieving a specic course
//1. Retrieve the course that matches the course ID provided from the URL

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	}).catch(error => error)
}


//UPDATE a course
/*
Steps:
	1. Create a variable "updatedCourse" which will contain the info retrieved from the req.body
	2. Find and Update the course using the courseId retrieved from the req.params and the variable "updatedCourse" containing info from req.body

*/

module.exports.updateProduct = (productId, data) => {
	//specify the fields/properties of the document to be updated
	let updatedProduct = {
		name: data.name,
		description: data.description,
		price: data.price
	}

	//findByIdAndUpdate(document Id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


//Archiving a course
//1. update the status of "isActive" into "false" which will no longer be displayed in the client whenever all active courses are retrieved.
module.exports.archiveProduct = (productId) => {
	let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}




//Activating a course
module.exports.activateProduct = (productId) => {
	let updateActiveField = {
		isActive: true
	};

	return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}
















































