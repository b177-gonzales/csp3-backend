//[SECTION] Dependencies and Modules
  const User = require('../models/User'); 
  const Product = require('../models/Product'); 
  const bcrypt = require('bcrypt');
  const dotenv = require('dotenv');
  const auth = require('../auth.js');

//[SECTION] Environment Variables Setup
    dotenv.config(); 
    const asin = Number(process.env.SALT); 
   
//[SECTION] Functionalities [CREATE]
	//1. Register New Account
	module.exports.register = (userData) => {
        let fName = userData.firstName;
        let lName = userData.lastName;
        let email = userData.email;
        let passW = userData.password;
        let mobil = userData.mobileNo;	

        let newUser = new User({
        	firstName: fName,
        	lastName: lName,
        	email: email,
        	password: bcrypt.hashSync(passW, asin), 
        	mobileNo: mobil
        }); 
       
		return newUser.save().then((user, err) => {
			if (user) {
				return user; 	
			} else {
				return {mesage: 'Failed to Register account'}; 
			}; 
		}); 
	}; 


//User Authentication
/*
Steps:
1. Check the database if the user email exists
2. Compare the password provided in the login form with password stored in the database.
3. Generate/return a JSON web token if the user is successfully logged in, and return false if not.

*/

module.exports.loginUser = (data) => {
		//findOne method returns the first record in the collection that matches the search criteria
		return User.findOne({ email: data.email }).then(result => {
				//User does not exist

				if(result == null) {
						return false;
				} else {
						//User exists
						//'compareSync' method from the bcrypt to used in comparing the non encrypted password from the login and the database password. it return 'true' or 'false' depending on the result
						const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

						//if the password match, return token
						if(isPasswordCorrect){
								return { accessToken: auth.createAccessToken(result.toObject()) }
						}else {
							//password do not match
							return false;
						}

				}
		})
}


//[SECTION] Functionalities [RETRIEVE] the user details

/*
Steps:
1. Find the document in the databse using the user's ID
2. Reassign the password of the returned document to an empty string.
3. Return the result back to the client
*/

module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
			//change the value of the user's password to an empty string
			result.password = '';
			return result;
	})
}


//Enroll registered user

/*
Enrollment Steps
	1. Look for the user by its ID.
			- push the details of the course we're trying to enroll in. We'll push the details to a new enrollment subdocument in our user.
	2. Look for the course by its ID.
			- push the details of the enrollee/user who's trying to enroll. We'll push to a new enrollees subdocument in our course.

	3. When both saving documents are successful, we send a message to a client. true if successful, false if not.
*/

module.exports.purchase = async (req, res) => {
		//console.log("Test enroll route");
		console.log(req.user.id);//the user's id from the decoded token after verify()
		console.log(req.body.productId); //the course ID from our request body

		//Process stops here and sends response if user is an admin
		if(req.user.isAdmin) {
				return res.send({ message: "Action Forbidden"})
		}

		//get the user's ID to save the courseId inside the enrollments field

		let isUserUpdated = await User.findById(req.user.id).then( user => {
							//add the courseId in an object and push that object into user's enrollments array:

							let newEnrollment = {
									productId: req.body.productId
							}

							user.enrollments.push(newEnrollment);

							//save the changes made to our user document
							return user.save().then(user => true).catch(err => err.message)

							//if isUserUpdated does not containt the boolean true, we will stop our process and return a message to our client
							if(isUserUpdated !== true) {
									return res.send({ message: isUserUpdated })
							}
		});

		//Find the course Id that we will need to push to our enrollee
		let isProductUpdated = await Product.findById(req.body.productId).then(product => {
					//create an object which will be push to enrollees array/field
					let enrollee = {
							userId: req.user.id
					}

					product.enrollees.push(enrollee);

					//save the course document
					return product.save().then(product => true).catch(err => err.message)


					if(isProductUpdated !== true) {
							return res.send({ message: isProductUpdated })
					}

		})


		//send message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true.

		if(isUserUpdated && isProductUpdated) {
				return res.send({ message: "Purchased Successfully" })
		}

}


//Get user's enrollments

module.exports.getOrders = (req, res) => {
		User.findById(req.user.id)
		.then(result => res.send(result.orders))
		.catch(err => res.send(err))
}
