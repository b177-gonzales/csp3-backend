const express = require('express');
const route = express.Router();
const ProductController = require('../controller/products')
const auth = require('../auth');

//destructure the actual function that we need to use

const { verify, verifyAdmin } = auth;

//Route for Creating a course
route.post('/create', verify, verifyAdmin, (req, res) => {
	ProductController.addProduct(req.body).then(result => res.send(result))
})


//Retrieve all courses
route.get('/all', (req, res) => {
	ProductController.getAllProducts().then(result => res.send(result));
})

//Retrieve all ACTIVE courses
route.get('/active', (req, res) => {
	ProductController.getAllActive().then(result => res.send(result));
})


//Retrieving a SPECIFIC course
//req.params (is short for a parameter)
// " /:parameterName"
route.get('/:productId', (req, res) => {
	console.log(req.params.productId)
	//we can retrieve the course ID by accessing the request's "params" property which contains all the paramteres provided via the URL
	ProductController.getProduct(req.params.productId).then(result => res.send(result));
})


//Route for UPDATING a course
route.put('/:productId', verify, verifyAdmin, (req, res) => {
	ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result))
})


//Archiving a course
route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
	ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
})


//Activate a course
route.put('/:productId/activate', verify, verifyAdmin, (req, res) => {
	ProductController.activateProduct(req.params.productId).then(result => res.send(result));
})


























module.exports = route;