//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controller/users'); 
	// We will import auth module so we can use our verify method as middleware for our routes
	const auth = require('../auth');

//[SECTION] Routing Component
	const route = exp.Router(); 

//[SECTION] Routes- POST
	route.post('/register', (req, res) => {
		console.log(req.body);
		let userData = req.body; 
		controller.register(userData).then(outcome => {
			res.send(outcome);
		});
	}); 
	
route.post('/login', (req, res) => {
	controller.loginUser(req.body).then(result => res.send(result));
})

route.get('/details', auth.verify, (req, res) => {
	controller.getProfile(req.user.id).then(result => res.send(result));
})

route.post('/purchase', auth.verify, controller.purchase);

route.get('/getOrders', auth.verify, controller.getOrders);

	module.exports = route; 