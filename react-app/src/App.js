import { useState } from 'react';
// for routing
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import SpecificProduct from './pages/SpecificProduct';
import { Container } from 'react-bootstrap'

import { UserProvider } from './UserContext';

function App() {


  // state the hook for the user state that defined the user details
  // the state of the user will be used to add a context as a global state in our app.
  // React context helps you broadcast the data and changes happening to the data.
  const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  // function for clearing localStorage on logout
  const unsetUser = () => {
      localStorage.clear();
  }

  // The BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser
  /*use the Routes component to render components within the container on the defined routes*/
  // Route specify the exact url in each component

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={ <Home /> } />
            <Route path="/products" element={ <Products /> }/>
            <Route path="/register" element={ <Register /> } />
            <Route path="/login" element={ <Login />  } />   
            <Route path="/logout" element={ <Logout />  } />   
            <Route path="/products/:productId" element={ <SpecificProduct />  } />   
          </Routes>
        </Container>     
      </Router>
    </UserProvider>
  );
}

export default App;
