import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';

export default function AddProduct({ fetchData }) {


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	// States for opening and closing our modal
	const [showAdd, setShowAdd] = useState(false);

	// Functions to hand opening and closing of our modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);


	// add a function

	const addProduct = (e) => {
		e.preventDefault();

		fetch("https://intense-sea-60933.herokuapp.com/products/create", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				alert("Product successfully added.")

				setName('');
				setDescription('');
				setPrice(0);

				// close our modal
				closeAdd();
				// to render the updated data, use the fetchData from the props
				fetchData();
				// alternative way to reload the data
				// window.location.href('/courses')
			}else {
				alert("Something went wrong, please try again")

				setName('');
				setDescription('');
				setPrice(0);

				closeAdd();
				fetchData();
			}


		})
	}




	return(
		<>
			<Button variant="warning" onClick={openAdd}>Add New Product</Button>

		{/* Add Modal */}

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add a Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text"
								value={name}
								onChange={e => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								value={description}
								onChange={e => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="text"
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit" onClick={addProduct}>Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>

		</>


		)
}








