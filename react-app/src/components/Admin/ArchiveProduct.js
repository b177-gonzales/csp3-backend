import { Button } from 'react-bootstrap';


export default function ArchiveProduct({ product, isActive, fetchData}) {

	const archiveToggle = (productId) => {
		fetch(`https://intense-sea-60933.herokuapp.com/products/${ productId }/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully disabled")
				fetchData()
			}else {
				alert("Something went wrong")
				fetchData()
			}
		})
	}


	const activateToggle = (productId) => {
		fetch(`https://intense-sea-60933.herokuapp.com/products/${ productId }/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully enabled")
				fetchData()
			}else {
				alert("Something went wrong")
				fetchData()
			}
		})
	}

	return(

		<>
			{isActive ?
				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Disable</Button>
				:
				<Button variant="success" size="sm" onClick={() => activateToggle(product)}>Enable</Button>
			}
			
			
		</>

		)
}