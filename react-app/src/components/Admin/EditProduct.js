import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';


export default function EditProduct({product, fetchData}) {

	// state for opening and closing the modal
	const [showEdit, setShowEdit] = useState(false);

	// state for the forms and courseId
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");


	// function for still getting the data to the form
	const openEdit = (productId) => {

		fetch(`https://intense-sea-60933.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			// Populate all input values with the course information that we fetched
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})

		// open the modal
		setShowEdit(true)
	}


	// function to close the edit modal
	const closeEdit = () => {
		// reset all input values in our state
		setShowEdit(false)
		setName('')
		setDescription('')
		setPrice('')
	}

	// function for API integration to update the course

	const editProduct = (e, courseId) => {
		e.preventDefault();

		fetch(`https://intense-sea-60933.herokuapp.com/products/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				alert("Product successfully updated");
				closeEdit();
				fetchData();
			}else {
				alert("Something went wrong");
				fetchData();
				closeEdit();
			}
		})
	}


	return(
		<>
			<Button variant="success" size="sm" onClick={() => openEdit(product)} >Update</Button>



		{/*Edit Modal*/}

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text"
								value={name}
								onChange={e => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								value={description}
								onChange={e => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="text"
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>
		)
}