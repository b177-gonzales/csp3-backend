import { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import AddProduct from './Admin/AddProduct';
import EditProduct from './Admin/EditProduct';
import ArchiveProduct from './Admin/ArchiveProduct';


export default function AdminView(props) {

	// deconstruct props
	const { productsData, fetchData } = props;

	const [products, setProducts] = useState([])


	useEffect(() => {

		const productsArr = productsData.map(product => {
			return(
				<tr key={product._id}>
					<td> { product._id } </td>
					<td> { product.name }  </td>
					<td> { product.description }  </td>
					<td> { product.price }  </td>
					<td className={product.isActive ? "text-success" : "text-danger"}> 
						{ product.isActive ? "Available" : "Unavailable"}  
					</td>
					<td>
						<EditProduct product={product._id} fetchData={fetchData}/>
								
						<ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/>
					</td>
					
				</tr>
				)
		})

		setProducts(productsArr);

	}, [productsData])


	return(
		<>
			<div className="text-center my-4">
				<h1>Admin Dashboard</h1>
				<AddProduct fetchData={fetchData}/>
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
					{ products }
				</tbody>
			</Table>
		</>

		)
}