import { useState, useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  // state to store the user information stored in the login page
  //const [user, setUser] = useState(localStorage.getItem('email'));

	return(

		      <Navbar bg="dark" expand="lg" variant="dark" className="mb-5">
            <Navbar.Brand as={ Link } to="/" className="ms-2">Store</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            	{/*ms-auto - s - (start) for classes that set margin-left and set the margin to auto*/}
              <Nav className="ms-auto"> 
                <Nav.Link as={ Link } to="/">Home</Nav.Link>
                <Nav.Link as={ Link } to="/products">Products</Nav.Link>
                { user.accessToken !== null ?
                    <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
                    :
                    <>
                      <Nav.Link as={ Link } to="/login">Login</Nav.Link>
                      <Nav.Link as={ Link } to="/register">Register</Nav.Link>
                    </>
                }
              </Nav>
            </Navbar.Collapse>
          </Navbar>
		)
}