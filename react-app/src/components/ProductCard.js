import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({ productProp }) {
	// props > object > object
	// props.courseProp.name
	// Deconstruct the course properties into their own variables
	const { _id, name, description, price } = productProp;
	// console.log(props);
	// console.log(typeof props);
	// console.log(typeof "string");

	// create state to track the enrollees
	// const [getter, setter] = useState(initialGetterValue)
	const [count, setCount] = useState(0);
	const [stocks, setStocks] = useState(100);

	// state for the button enroll
	const [isOpen, setIsOpen] = useState(true);

	function enroll() {
			setCount(count + 1);
			setStocks(stocks - 1);
	}


	useEffect(() => {
		if(stocks === 0) {
			setIsOpen(false);
		}
	}, [stocks]);

	// important note for use effect
		// side effects happen "after render"
		// useEffect is like an event listener, it triggers the useEffect whenever states have a rapid changes
	// Fetch if we want na ma render yung updated data.
	//  we used the useEffect if we want to render(catch the rapid changes)

	// useEffect if the optional dependency array is empty ([]), the useEffect will run ONLY on initial render(once).
	// If we want to control the rendering of our useEffect we add states to our optional dependency array to control when useEffect will run


	return (
			<Card>
			    <Card.Body>
			        <Card.Title> { name } </Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text> { description } </Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>PhP {price} </Card.Text>
			        <Card.Text>Purchases: {count} </Card.Text>
			        <Card.Text>Available Stock: {stocks} </Card.Text>

			        
			        <Link className="btn btn-info" to={`/products/${_id}`}>Details</Link>
			        

			        
			        
			    </Card.Body>
			 </Card>
			)
}