import { useEffect, useState } from 'react';
import ProductCard from './ProductCard';



export default function UserView({ productsData }) {

	console.log(productsData)

	const [products, setProducts] = useState([])

	useEffect(() => {

		const productsArr = productsData.map(product => {
			// only render the active courses
			if(product.isActive === true){
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			} else {
				return null;
			}
		})

		// set the courses state to the result of our map function
		setProducts(productsArr)

	}, [productsData])

	return(
		<>
			{ products }
		</>


		)
}