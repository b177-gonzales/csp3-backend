import AppNavbar from '../components/AppNavbar';
import Banner from '../components/Banner';
import Products from './Products';

export default function Home() {
	return (

		<>
			<Banner />
			<Products />
		</>

		)
}