import React, { useState, useEffect, useContext } from 'react';
//useContext is used to unpack or deconstruct the value of the UserContext
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';


export default function Login() {

	const navigate = useNavigate();

	// fetch() is a method which allows to send a request to an api and process its response.

	//fetch('url', {}).then(res => res.json()).then(result => {})
	// url from the route of API
	// optional object contains the additional information about our request such as the method, the body and the headers.
	//.then(res => res.json()) => parse the response as JSON
	// .then(result => {}) => this is where we get the actual data/ we can also manipulate the data



	// unpacking/deconstructing the object that we need inside the UserContext
	const { setUser, user } = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	//login button
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		//Validation to enable submit button 
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])


	function authenticate(e) {

		e.preventDefault();

		fetch('https://intense-sea-60933.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {	
			console.log(data);

			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({
					accessToken: data.accessToken
				})

				alert("You are now login to our website")

				fetch('https://intense-sea-60933.herokuapp.com/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)

					if(result.isAdmin === true) {
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							isAdmin: result.isAdmin
						})

						navigate('/products')
					}else {

						navigate('/products')
					}

				})

			}else {
				alert("Error, something went wrong, check your credentials")
			}

			// Clear inputs
			setEmail('');
			setPassword('');
		})
		
	}
	
	return (
		user.accessToken !== null ?

			<Navigate to="/products" />
			:

			<Form onSubmit={(e) => authenticate(e)}>
				<h1>Login</h1>
				<Form.Group>
					<Form.Label>Email address:</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter email"
						required
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						autoComplete="true"

					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter your password"
						required
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						autoComplete="true"
						
					/>
				</Form.Group>

				{isActive ?
					<Button variant="primary" type="submit" className="mt-3"> Login </Button>

					:

					<Button variant="danger" type="submit" disabled className="mt-3"> Login </Button>
				}

				
			</Form>
		)
}