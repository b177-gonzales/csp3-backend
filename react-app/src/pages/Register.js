import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Register() {
    // Two Way binding
    // The data we changed in the view has updated the state
    // the data in the state has updated the view

    // state hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');

    // state to determine whether submit button is enabled or not for conditional rendering
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match

        if((email !== '' && password !== '' && password2 !== '') && (password === password2)){
            setIsActive(true);
        }else {
            setIsActive(false);
        }

    }, [email, password, password2]);

    function registerUser(e) {
        // prevent page redirection via form submission
        e.preventDefault();

        // clear input fields
        setEmail('');
        setPassword('');
        setPassword2('');

        alert('Thank you for registering!')

    }

    return (

        <Form onSubmit={e => registerUser(e)}>
            <h1>Register</h1>
            <Form.Group>
                <Form.Label> First Name: </Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter First Name"
                    required
                    value={firstName}
                    onChange={e => setFirstName(e.target.value) }
                    // e.target.value = property allows us to gain access to the input field's current value
                 />
            </Form.Group>

            <Form.Group>
                <Form.Label> Last Name: </Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter Last Name"
                    required
                    value={lastName}
                    onChange={e => setLastName(e.target.value) }
                    // e.target.value = property allows us to gain access to the input field's current value
                 />
            </Form.Group>

            <Form.Group>
                <Form.Label> Email: </Form.Label>
                <Form.Control 
                    type="email"
                    placeholder="Enter email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value) }
                    // e.target.value = property allows us to gain access to the input field's current value
                 />
            </Form.Group>

            <Form.Group>
                <Form.Label> Password: </Form.Label>
                <Form.Control 
                    type="password"
                    placeholder="Enter password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value) }
                 />
            </Form.Group>

            <Form.Group>
                <Form.Label> Verify Password: </Form.Label>
                <Form.Control 
                    type="password"
                    placeholder="Verify password"
                    required
                    value={password2}
                    onChange={e => setPassword2(e.target.value) }
                 />
            </Form.Group>
            {isActive ?
                <Button variant="primary" type="submit">Submit</Button>
                :
                <Button variant="primary" type="submit" disabled>Submit</Button>
            }

            
            

        </Form>

        )
}