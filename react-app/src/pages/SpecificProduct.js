import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button} from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function SpecificProduct() {

	const navigate = useNavigate();

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

// useParams() contains any value that we are trying to pass in the URL stored in a wildcard paramater from the App.js
	const { productId } = useParams();

	useEffect(() => {

		fetch(`https://intense-sea-60933.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [])

	const purchase = (productId) => {
		fetch('https://intense-sea-60933.herokuapp.com/users/purchase', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("You have successfully purchased this item.")
				navigate('/products')
			} else {
				alert("Something went wrong, please try again")
			}
		})
	}


	return(

		<Container>
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
				</Card.Body>
				<Card.Footer>

					{
						user.accessToken !== null ?
						<Button variant="primary" onClick={() => purchase(productId)}>Purchase</Button>
						:
						<Link className="btn btn-warning" to="/login">Login to Purchase</Link>
					}
					
				</Card.Footer>
			</Card>
		</Container>

		)
}